<?php

function download_image($config, $target_file)
{
    $download_image = $config["url"];
    $folder = $config["folder_name"];
    $image_id = 'bio_big';

    // Store the original filename
    $original_name = basename($download_image); // "img1.jpg"
    // Original extension by string manipulation
    $original_extension = substr($original_name, strrpos($original_name, '.')); // ".jpg"
    // Get the file and save it
    $img = file_get_contents($download_image);
    // https://images.psmcdn.net/design/tour/fs/tour/pics/honey_blossom_and_savannah_sixx/bio_big.jpg
    $path = 'assets/tour/' . $config['site'] . '/tour/pics/' . $folder;
    $stored_name = $path . '/' . $image_id . $original_extension;
    // $stored_name = 'assets/' . $config['site'] . '/' . $folder . '/' . 'shared/bio_big.jpg';
    // echo $stored_name."<br>";
    if ($img) {
        if (!file_exists($path)) {
            mkdir($path, 0777, true);
            file_put_contents($stored_name, $img);
        } else {
            file_put_contents($stored_name, $img);
        }
    } else {
        // Error, couldn't get file
        echo 'Something went wrong...';
    }
}

function getApi($siteName, $curr_page)
{
    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => "http://stage.lumina.paperskeet.com/api/v1/scenes.json?properties[]=cdnUniqueName&site.shortName=".$siteName."&psize=100&page=".$curr_page,
        // CURLOPT_URL => "http://dev.lumen.paperskeet.com/rest/teamskeetv1/msite/84000/192.169.1.1?site=" .  . "&sitename=TsNetwork",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
            "cache-control: no-cache"
        ),
    ));

    $response = curl_exec($curl);
    $response = json_decode(json_encode($response), FALSE);
    $err = curl_error($curl);

    curl_close($curl);

    if ($response) {
        return $response;
    } else {
        return $err;
    }
}

function readModels($models, $target_file)
{
    foreach ($models as $model) {
        $img = "https://images.psmcdn.net/teamskeet/" . $_GET["site"] . "/" . $model->cdnUniqueName . "/members" . "/" . $target_file . ".jpg";
        $config = [
            "url" => $img,
            "site" => $_GET["site"],
            "folder_name" => $model->cdnUniqueName
        ];
        download_image($config, $target_file);
    }
}

if ($_GET["site"] && $_GET["filename"] && $_GET["page"]) {
    $site = $_GET["site"];
    $target_file = $_GET["filename"];
    $page = $_GET["page"];
    $get_models = getApi($site, $page);
    readModels(json_decode($get_models), $target_file);
} else {
    die('Pls add site and filename parameters.');
}
